// $Id$

mgEveItemPrices - Eve Online price item price caching via eve-central.com

----------------------------------------------------


Known incompatibilities
-----------------------

No incompatibilities with other modules are known to this date.


Maintainers
-----------
mgEveItemPrices is written by Michael Lehman - Drakos Wraith in game.


